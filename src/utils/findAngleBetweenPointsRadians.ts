export const findAngleBetweenPointsRadians = (sx: number, sy: number, ex: number, ey: number) => {
    return Math.atan2((ey - sy) , (ex - sx));
}
