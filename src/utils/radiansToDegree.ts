export const radiansToDegree = (radians: number) => {
    return radians * 180 / Math.PI;
}

export const degreeToRadians = (degree: number) => {
    return degree * Math.PI / 180;
}
