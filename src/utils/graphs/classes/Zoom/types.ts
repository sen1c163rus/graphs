import {Canvas} from "../Canvas";

export type Props = {
  canvas: Canvas;
}
