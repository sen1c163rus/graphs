import {Lifecycle} from "../../../types";
import {Props} from "./types";

export class Zoom implements Lifecycle {
  #isInit = false;
  #zoom: number = 1;
  #canvas: Props['canvas'];

  constructor({ canvas }: Props) {
    this.#canvas = canvas;
  }

  #handlerWheel(event: WheelEvent) {
    event.preventDefault();
    const delta = Math.sign(event.deltaY);
    if (delta > 0 && this.#zoom > 0.2) {
      this.#zoom -= 0.1;
    } else if (delta < 0 && this.#zoom < 10) {
      this.#zoom += 0.1;
    }
  }

  public init() {
    this.#canvas.element.addEventListener('wheel', this.#handlerWheel.bind(this));
    this.#isInit = true;
  }

  get isInit() {
    return this.#isInit;
  }

  get zoom() {
    return this.#zoom;
  }

  public destroy() {
    this.#zoom = 1;
    this.#canvas.element.removeEventListener('wheel', this.#handlerWheel.bind(this));
    this.#isInit = false;
  }
}
