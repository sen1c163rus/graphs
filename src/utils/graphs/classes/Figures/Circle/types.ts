import {Figure} from "../../../../types";
import {Point} from "../../../../types/point";

export type Props = {
  name: Figure['name'];
  position: Point;
  radius: number;
}
