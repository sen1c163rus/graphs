import {DrawProps, Figure} from "../../../../types";
import {Props} from "./types";

export class Circle implements Figure {
  readonly #name: Props['name'];
  #position: Props['position'];
  #radius: Props['radius'];

  constructor({name, position, radius}: Props) {
    this.#name = name;
    this.#position = position;
    this.#radius = radius;
  }

  public draw({canvas}: DrawProps) {
    const {context2D} = canvas;
    context2D.beginPath();
    context2D.arc(this.#position.x, this.#position.y, this.#radius, 0, 2 * Math.PI, false);
    context2D.fillStyle = 'black';
    context2D.fill();
    context2D.closePath();
  }

  get name() {
    return this.#name;
  }

  get position() {
    return this.#position;
  }

  set position(position: Props['position']) {
    this.#position = position;
  }

  get radius() {
    return this.#radius;
  }
}
