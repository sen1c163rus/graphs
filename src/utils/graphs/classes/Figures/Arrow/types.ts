import {Figure} from "../../../../types";
import {Point} from "../../../../types/point";
import {Canvas} from "../../Canvas";

export type Props = {
  name: Figure['name'];
  isCouple?: boolean;
  from: string;
  to: string;
}

export type DrawArrowProps = {
  angleFromBetweenVectorsDegree: number;
  angleToBetweenVectorsDegree: number;
  canvas: Canvas;
}

export type DrawSingleArrowProps = DrawArrowProps;
export type DrawCoupleArrowProps = DrawArrowProps;

export type GetPointsArrowProps = {
  angleFromBetweenVectorsDegree: number;
  angleToBetweenVectorsDegree: number;
  degreeDeviation?: number;
  arrowHeadLength: number;
};

export type GetQuadraticPointProps = {
  pointFrom: Point;
  pointTo: Point;
}

export type DrawArrowHeadProps = {
  pointFrom: Point;
  pointTo: Point;
  width: number;
  height: number;
  canvas: Canvas;
}
