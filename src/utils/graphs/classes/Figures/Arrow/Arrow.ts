import {DrawProps, Figure} from "../../../../types";
import {DrawArrowHeadProps, DrawCoupleArrowProps, DrawSingleArrowProps, GetPointsArrowProps, Props} from "./types";
import {storeArrows} from "./storeArrow";
import {Point} from "../../../../types/point";
import {degreeToRadians, radiansToDegree} from "../../../../radiansToDegree";
import {findAngleBetweenPointsRadians} from "../../../../findAngleBetweenPointsRadians";
import {Circle} from "../Circle";

export class Arrow implements Figure {
  #name: Props['name'];
  #isCouple: Props['isCouple'];
  #from: Props['from'];
  #to: Props['to'];
  #circleFrom: Circle | undefined;
  #circleTo: Circle | undefined;
  #isDouble = false;
  #arrowSize = {
    width: 15,
    height: 15,
  };

  constructor({name, isCouple, from, to}: Props) {
    this.#name = name;
    this.#isCouple = isCouple;
    this.#from = from;
    this.#to = to;
    this.#checkCouple();
  }

  public draw({canvas, graphs}: DrawProps) {
    const circleFrom = graphs.circles.get(this.#from);
    const circleTo = graphs.circles.get(this.#to);

    if (!circleFrom || !circleTo) {
      if (!circleFrom && !circleTo) {
        console.error(`Canvas not drew "${this.#from}" and "${this.#to}" circles`);
        return;
      }

      if (!circleFrom) {
        console.error(`Canvas not drew "${this.#from}" circle`);
        return;
      }

      console.error(`Canvas not drew "${this.#to}" circle`);
      return;
    }

    this.#circleFrom = circleFrom;
    this.#circleTo = circleTo;

    const {position: {x: xFrom, y: yFrom}, radius: radiusFrom} = this.#circleFrom;
    const {position: {x: xTo, y: yTo}, radius: radiusTo} = this.#circleTo;

    const angleToBetweenVectorsDegree =
      this.#getAngleBetweenVectorsDegree(
        {x: xFrom, y: yFrom},
        {x: xTo, y: yTo},
        {x: xTo + radiusTo, y: yTo}
      );

    const angleFromBetweenVectorsDegree =
      this.#getAngleBetweenVectorsDegree(
        {x: xTo, y: yTo},
        {x: xFrom, y: yFrom},
        {x: xFrom + radiusFrom, y: yFrom}
      );

    const drawFunc = this.#isCouple ? this.#drawCoupleArrow.bind(this) : this.#drawSingleArrow.bind(this);

    drawFunc({
      angleFromBetweenVectorsDegree,
      angleToBetweenVectorsDegree,
      canvas,
    });
  }

  #drawSingleArrow(
    {
      angleToBetweenVectorsDegree,
      angleFromBetweenVectorsDegree,
      canvas,
    }: DrawSingleArrowProps) {
    const {pointsVector: {from, to}} = this.#getPointsVector({
      angleToBetweenVectorsDegree,
      angleFromBetweenVectorsDegree,
      arrowHeadLength: 50,
    });

    const {context2D} = canvas;

    context2D.beginPath();
    context2D.moveTo(from.x, from.y);
    context2D.lineTo(to.x, to.y);
    context2D.strokeStyle = 'red';
    context2D.lineWidth = 3;
    context2D.stroke();
    context2D.closePath();

    this.#drawArrowHead({
      width: this.#arrowSize.width,
      height: this.#arrowSize.height,
      pointFrom: from,
      pointTo: to,
      canvas,
    });
  }

  #drawCoupleArrow(
    {
      angleToBetweenVectorsDegree,
      angleFromBetweenVectorsDegree,
      canvas,
    }: DrawCoupleArrowProps) {
    const degreeDeviation = 32;

    const {pointsVector: {from, to}} = this.#getPointsVector({
      angleToBetweenVectorsDegree,
      angleFromBetweenVectorsDegree,
      degreeDeviation,
      arrowHeadLength: 50,
    });

    const {context2D} = canvas;

    context2D.beginPath();
    context2D.moveTo(from.x, from.y);
    context2D.lineTo(to.x, to.y);
    context2D.strokeStyle = 'black';
    context2D.lineWidth = 3;
    context2D.stroke();
    context2D.closePath();

    this.#drawArrowHead({
      width: this.#arrowSize.width,
      height: this.#arrowSize.height,
      pointFrom: from,
      pointTo: to,
      canvas,
    });
  }

  #getPointsVector({
    angleToBetweenVectorsDegree,
    angleFromBetweenVectorsDegree,
    degreeDeviation: degreeDeviationProps,
  }: GetPointsArrowProps) {
    if (!this.#circleFrom || !this.#circleTo) {
      throw new Error('CircleFrom or CircleTo is not initialized');
    }
    const degreeDeviation = degreeDeviationProps || 0;

    const from = this.#circleFrom;
    const pointFrom: Point = {
      x: from.radius * Math.cos(degreeToRadians(angleFromBetweenVectorsDegree + degreeDeviation)) + from.position.x,
      y: from.radius * Math.sin(degreeToRadians(angleFromBetweenVectorsDegree + degreeDeviation)) + from.position.y,
    };

    const to = this.#circleTo;
    const pointTo: Point = {
      x: to.radius * Math.cos(degreeToRadians(angleToBetweenVectorsDegree - degreeDeviation)) + to.position.x,
      y: to.radius * Math.sin(degreeToRadians(angleToBetweenVectorsDegree - degreeDeviation)) + to.position.y,
    };

    const pointVectorTo = this.#decreaseVectorLength(pointFrom, pointTo, this.#arrowSize.height);

    return {
      pointsVector: {
        from: pointFrom,
        to: pointVectorTo,
      },
      pointsArrow: {
        from: pointVectorTo,
        to: pointTo,
      }
    }
  }

  #decreaseVectorLength(start: Point, end: Point, length: number) {
    // Calculate the original vector
    const originalVector = {
      x: end.x - start.x,
      y: end.y - start.y
    };

    // Calculate the length of the original vector
    const originalLength = Math.sqrt(originalVector.x * originalVector.x + originalVector.y * originalVector.y);

    // Calculate the unit vector
    const unitVector = {
      x: originalVector.x / originalLength,
      y: originalVector.y / originalLength
    };

    // Multiply the unit vector by the desired length to obtain the new vector
    const reducedVector = {
      x: unitVector.x * (originalLength - length),
      y: unitVector.y * (originalLength - length)
    };

    // Add the reduced vector to the starting point to obtain the new ending point
    return {
      x: start.x + reducedVector.x,
      y: start.y + reducedVector.y
    };
  }

  #drawArrowHead({width, height, pointFrom, pointTo, canvas}: DrawArrowHeadProps) {
    const {context2D} = canvas;
    context2D.save();

    context2D.translate(pointTo.x, pointTo.y);
    context2D.rotate(findAngleBetweenPointsRadians(pointFrom.x, pointFrom.y, pointTo.x, pointTo.y));

    context2D.beginPath();
    context2D.fillStyle = "red";
    context2D.moveTo(0, 0);
    context2D.lineTo(0, height / 2);
    context2D.lineTo(width, 0);
    context2D.lineTo(0, -height / 2);
    context2D.closePath();
    context2D.fill();

    context2D.restore();
  }

  #getAngleBetweenVectorsDegree(
    from: Point,
    middle: Point,
    to: Point,
  ) {
    const scalar1: Point = {
      x: from.x - middle.x,
      y: from.y - middle.y,
    };

    const scalar2: Point = {
      x: to.x - middle.x,
      y: to.y - middle.y,
    };

    const dot = scalar1.x * scalar2.x + scalar1.y * scalar2.y;
    const cross = scalar1.y * scalar2.x - scalar1.x * scalar2.y;

    const angleRadians = Math.atan2(cross, dot);
    const angleDegree = radiansToDegree(angleRadians);

    return angleDegree >= 0 ? angleDegree : angleDegree + 360;
  }

  #checkCouple() {
    if (!this.#isCouple) return;

    if (storeArrows.has(this.#to)) {
      this.#isDouble = true;
      return;
    }

    storeArrows.set(this.#from, this.#to);
  }

  get name() {
    return this.#name;
  }
}
