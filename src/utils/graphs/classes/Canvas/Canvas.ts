import {Lifecycle} from "../../../types";
import { Props } from './types';
import {DEFAULT_SIZES_CANVAS} from "./constants";

export class Canvas implements Lifecycle {
  #isInit = false;
  readonly #containerId: Props['containerId'];
  #canvasContainer: HTMLDivElement;
  #canvas: HTMLCanvasElement;
  #context2D: CanvasRenderingContext2D;
  #canvasWidth: number = DEFAULT_SIZES_CANVAS.width;
  #canvasHeight: number = DEFAULT_SIZES_CANVAS.height;

  constructor({ containerId }: Props) {
    this.#containerId = containerId;

    const foundContainer = document.getElementById(containerId) as HTMLDivElement | null;
    if (!foundContainer) {
      throw new Error('Container not found');
    }

    this.#canvasContainer = foundContainer;
    this.#canvas = document.createElement('canvas');
    const context2D = this.#canvas.getContext('2d');

    if (!context2D) {
      throw new Error('Context2D not supported');
    }

    this.#context2D = context2D;
  }

  public init() {
    this.#canvas.style.display = 'block';
    this.setSize(this.#canvasWidth, this.#canvasHeight);
    this.#canvasContainer.appendChild(this.#canvas);
    this.#isInit = true;
  }

  get isInit() {
    return this.#isInit;
  }

  public setSize(width: number, height: number) {
    this.#canvasWidth = width;
    this.#canvasHeight = height;

    this.#canvas.style.width = `${this.#canvasWidth}px`;
    this.#canvas.style.height = `${this.#canvasHeight}px`;

    this.#canvas.width = this.#canvasWidth;
    this.#canvas.height = this.#canvasHeight;
  }

  get containerId() {
    return this.#containerId;
  }

  get element() {
    return this.#canvas;
  }

  get context2D() {
    return this.#context2D;
  }

  get width() {
    return this.#canvasWidth;
  }

  get height() {
    return this.#canvasHeight;
  }

  public destroy() {
    if (this.#canvasContainer.contains(this.#canvas)) {
      this.#canvasContainer.removeChild(this.#canvas);
    }
    this.setSize(DEFAULT_SIZES_CANVAS.width, DEFAULT_SIZES_CANVAS.height);
    this.#isInit = false;
  }
}
