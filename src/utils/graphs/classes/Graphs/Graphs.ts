import {Figure, Lifecycle} from "../../../types";
import {Props} from "./types";
import {Arrow, Circle} from "../Figures";

export class Graphs implements Lifecycle {
  readonly #canvas: Props['canvas'];
  #isInit = false;
  #circles = new Map<string, Circle>();
  #arrows = new Map<string, Arrow>();

  constructor({ canvas }: Props) {
    this.#canvas = canvas;
  }

  public init() {
    this.#isInit = true;
  }

  public draw(figure: Figure) {
    figure.draw({ canvas: this.#canvas, graphs: this });

    if (figure instanceof Circle) {
      this.#circles.set(figure.name, figure);
    } else if (figure instanceof Arrow) {
      this.#arrows.set(figure.name, figure);
    }
  }

  get circles() {
    return this.#circles;
  }

  get isInit() {
    return this.#isInit;
  }

  public destroy() {
    this.#circles.clear();
    this.#arrows.clear();
    this.#isInit = false;
  }
}
