import {Lifecycle} from "../../../types";
import {Point} from "../../../types/point";
import {Props} from "./types";

export class Drag implements Lifecycle {
  #isInit = false;
  #canvas: Props['canvas'];
  #graphs: Props['graphs'];
  #zoom: Props['zoom'];
  #isDragging = false;
  #dragStart: Point = { x: 0, y: 0 };
  #offset: Point = { x: 0, y: 0 };

  constructor({ canvas, graphs, zoom }: Props) {
    this.#canvas = canvas;
    this.#graphs = graphs;
    this.#zoom = zoom;
  }

  #handlerMouseDown(event: MouseEvent) {
    this.#isDragging = true;
    this.#dragStart.x = event.clientX;
    this.#dragStart.y = event.clientY;
  }

  #handlerMouseMove(event: MouseEvent) {
    if (this.#isDragging) {
      this.#offset.x = event.clientX - this.#dragStart.x;
      this.#offset.y = event.clientY - this.#dragStart.y;
    }
  }

  #handlerMouseUp() {
    this.#graphs.circles.forEach((circle) => {
      circle.position.x = circle.position.x + this.#offset.x / this.#zoom.zoom;
      circle.position.y = circle.position.y + this.#offset.y / this.#zoom.zoom;
    })
    this.#offset = { x: 0, y: 0 };
    this.#isDragging = false;
  }

  public init() {
    this.#canvas.element.addEventListener('mousedown', this.#handlerMouseDown.bind(this));
    this.#canvas.element.addEventListener('mousemove', this.#handlerMouseMove.bind(this));
    this.#canvas.element.addEventListener('mouseup', this.#handlerMouseUp.bind(this));
    this.#isInit = true;
  }

  get isInit() {
    return this.#isInit;
  }

  get offset() {
    return this.#offset;
  }

  public destroy() {
    this.#canvas.element.removeEventListener('mousedown', this.#handlerMouseDown.bind(this));
    this.#canvas.element.removeEventListener('mousemove', this.#handlerMouseMove.bind(this));
    this.#canvas.element.removeEventListener('mouseup', this.#handlerMouseUp.bind(this));
    this.#isInit = false;
  }
}
