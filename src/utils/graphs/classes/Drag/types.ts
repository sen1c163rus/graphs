import {Canvas} from "../Canvas";
import {Graphs} from "../Graphs";
import {Zoom} from "../Zoom";

export type Props = {
  canvas: Canvas;
  graphs: Graphs;
  zoom: Zoom;
};
