export const getScreenSizes = () => {
  const { width: widthPX, height: heightPX } = window.getComputedStyle(document.documentElement);

  return {
    width: Number(widthPX.split('px')[0]),
    height: Number(heightPX.split('px')[0]),
  }
}
