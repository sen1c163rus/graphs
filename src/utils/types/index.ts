export type { Lifecycle } from './lifecycle';
export type { Figure, DrawProps } from './figure';
