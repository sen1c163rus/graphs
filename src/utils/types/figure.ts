import {Canvas, Graphs} from "../graphs/classes";

export type DrawProps = {
  canvas: Canvas;
  graphs: Graphs;
}

export interface Figure {
  name: string;
  draw: (props: DrawProps) => void;
}
