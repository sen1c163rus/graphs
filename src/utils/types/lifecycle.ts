export interface Lifecycle {
  init: () => void;
  destroy: () => void;
  isInit: boolean;
}
