import React, {useEffect} from 'react';
import {Arrow, Canvas, Circle, Drag, Graphs, storeArrows, Zoom} from "./utils/graphs/classes";
import {getScreenSizes} from "./utils/getScreenSizes";

function App() {
  useEffect(() => {
    const canvas = new Canvas({containerId: 'canvas_container'});
    const graphs = new Graphs({canvas});
    const zoom = new Zoom({canvas});
    const drag = new Drag({canvas, graphs, zoom});

    canvas.init();
    graphs.init();
    zoom.init();
    drag.init();

    const {context2D} = canvas;

    const screenSizes = getScreenSizes();
    canvas.setSize(screenSizes.width, screenSizes.height);

    const circle1 = new Circle({ position: {x: 200, y: 300}, radius: 20, name: 'circle1' });
    const circle2 = new Circle({ position: {x: 554, y: 533}, radius: 20, name: 'circle2' });
    const circle3 = new Circle({ position: {x: 354, y: 133}, radius: 20, name: 'circle3' });

    const arrow = new Arrow({ from: 'circle1', to: 'circle2', name: 'arrow1', isCouple: true });
    const arrow2 = new Arrow({ from: 'circle2', to: 'circle1', name: 'arrow2', isCouple: true });
    const arrow3 = new Arrow({ from: 'circle2', to: 'circle3', name: 'arrow3', isCouple: false });
    const arrow4 = new Arrow({ from: 'circle1', to: 'circle3', name: 'arrow4', isCouple: true });
    const arrow5 = new Arrow({ from: 'circle3', to: 'circle1', name: 'arrow4', isCouple: true });

    const drawing = () => {
      context2D.clearRect(0, 0, canvas.width, canvas.height);
      context2D.save();
      context2D.setTransform(zoom.zoom, 0, 0, zoom.zoom, drag.offset.x, drag.offset.y);
      graphs.draw(circle1);
      graphs.draw(circle2);
      graphs.draw(circle3);
      graphs.draw(arrow);
      graphs.draw(arrow2);
      graphs.draw(arrow3);
      graphs.draw(arrow4);
      graphs.draw(arrow5);
      context2D.restore();

      requestAnimationFrame(drawing);
    }

    drawing();

    return () => {
      canvas.destroy();
      graphs.destroy();
      zoom.destroy();
      drag.destroy();
      storeArrows.clear();
    }
  }, []);

  return (
    <div id="canvas_container"/>
  );
}

export default App;
